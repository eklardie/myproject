﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                Page.Validate();
                CLogin myInfo = (CLogin)Session["myLogin"];
                LinkButton1.Text = null;
            }
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }

    }

    protected void btnQuoteNumberSearch_Click(object sender, EventArgs e)
    {
        int searchQuery = Convert.ToInt32(txtQuoteNumberSearch.Text.Trim());
        string newCommand = "SELECT QUOTE_Number, QUOTE_ProjectName, QUOTE_ProjectNameTwo, QUOTE_ProjectAddress, QUOTE_ProjectAddressTwo, " +
                            "QUOTE_City, QUOTE_State, QUOTE_TotalPrice FROM QUOTES WHERE QUOTE_Number = @searchQuery";
        DataTable tbl = (CMethods.returnTable(newCommand, "@searchQuery", searchQuery));
        txtQuoteNumberSearch.Text = null;
        if (tbl.Rows.Count > 0)
        {
            string appendMe = null;
            foreach (DataRow row in tbl.AsEnumerable())
            {
                string projectName = row[1].ToString();
                string quoteNumber = row[0].ToString();
                string projectNameTwo = row[2].ToString();
                string projectAddress = row[3].ToString();
                string projectAddressTwo = row[4].ToString();
                string projectCity = row[5].ToString();
                string projectState = row[6].ToString();
                string projectPrice = ((decimal)row[7]).ToString("C");

                LinkButton1.Text = "QUOTE # " + quoteNumber + "     " + projectName + " : " + projectNameTwo + " ---- " + projectAddress + "  " + projectAddressTwo + "  " + projectCity + ", " + projectState + "  " + projectPrice;
                appendMe = quoteNumber;
            }
            Session.Add("link", appendMe);
        }
        else
        {
            LinkButton1.Text = "NO SEARCH RESULTS FOUND FOR YOUR REQUEST OF" + searchQuery.ToString();
            
        }

    }
    protected void btnProjectNameSearch_Click(object sender, EventArgs e)
    {
        string searchQuery = txtProjectNameSearch.Text.Trim();
        string newCommand = "SELECT QUOTE_Number, QUOTE_ProjectName, QUOTE_ProjectNameTwo, QUOTE_ProjectAddress, QUOTE_ProjectAddressTwo, " +
                            "QUOTE_City, QUOTE_State, QUOTE_TotalPrice FROM QUOTES WHERE QUOTE_ProjectName LIKE @searchQuery UNION " +
                            "SELECT QUOTE_Number, QUOTE_ProjectName, QUOTE_ProjectNameTwo, QUOTE_ProjectAddress, QUOTE_ProjectAddressTwo, " +
                            "QUOTE_City, QUOTE_State, QUOTE_TotalPrice FROM QUOTES WHERE QUOTE_ProjectNameTwo LIKE @searchQueryTwo";
        DataTable tbl = (CMethods.returnTable(newCommand, "@searchQuery", "%" + searchQuery + "%", "@searchQueryTwo", "%" + searchQuery + "%"));
        txtProjectNameSearch.Text = null;
        if (tbl.Rows.Count == 1)
        {
            string appendMe = null;
            foreach (DataRow row in tbl.AsEnumerable())
            {
                string projectName = row[1].ToString();
                string quoteNumber = row[0].ToString();
                string projectNameTwo = row[2].ToString();
                string projectAddress = row[3].ToString();
                string projectAddressTwo = row[4].ToString();
                string projectCity = row[5].ToString();
                string projectState = row[6].ToString();
                string projectPrice = ((decimal)row[7]).ToString("C");

                LinkButton1.Text = "QUOTE # " + quoteNumber + "     " + projectName + " : " + projectNameTwo + " ---- " + projectAddress + "  " + projectAddressTwo + "  " + projectCity + ", " + projectState + "  " + projectPrice;
                appendMe = quoteNumber;
            }
            Session.Add("link", appendMe);
        }
        else if(tbl.Rows.Count>1)
        {
            GVSearchResults.DataSource = tbl;
            GVSearchResults.DataBind();
            LinkButton1.Text = "TOO MANY RESULTS FOUND PLEASE NARROW YOUR SEARCH";
        }
        else 
        {
            LinkButton1.Text = "NO SEARCH RESULTS FOUND FOR YOUR REQUEST OF" + searchQuery.ToString();
        }

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if (Session["link"] != null)
        {
            Response.Redirect("Quotation.aspx?Quote=" + Session["link"].ToString(), true);
        }
        else { };
    }
}