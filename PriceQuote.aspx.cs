﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PriceQuote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                fillGVQuotes();
            }
        }
        else Response.Redirect("~/Default.aspx");
    }

    private void fillGVQuotes()
    {
        string sql = "SELECT * FROM QUOTES ORDER BY QUOTE_Number";
        GVQuoteItems.DataSource = CMethods.returnTable(sql);
        GVQuoteItems.AutoGenerateColumns = false;
        
        GVQuoteItems.DataBind();
    }
}