﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class CreateQuote : System.Web.UI.Page
{
    //public string selectedstate = "";
    //public string selectedcity = "";
    //public string selectedproject = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                Page.Validate();

                //string newCommand = "SELECT COUNT(q.QUOTE_ID),SUM(q.QUOTE_TotalPrice),u.USER_Initials " +
                //                    "FROM QUOTES q JOIN USERS u ON q.QUOTE_USERS_FK = u.USER_ID " +
                //                    "WHERE q.QUOTE_DueDate >= Convert(datetime, @date) " +
                //                    "GROUP BY u.USER_Initials";
                //DataTable tbl = CMethods.returnTable(newCommand,"@date","'"+DateTime.Now.AddMonths(-3).ToShortDateString()+"'");



            }
            fillQuotes(gvProjects,true);
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
        
    }

    private void getAllItems()
    {
        DataSet mydata = new DataSet();
        

        //DataSet quotes = new DataSet();
        //DataTable dt = quotes.QUOTES.CopyToDataTable<DataRow>();
        //ReportDataSource rds = new ReportDataSource("QuoteData", dt);
        //ReportViewer1.LocalReport.DataSources.Clear();
        //ReportViewer1.LocalReport.DataSources.Add(rds);
        //ReportViewer1.LocalReport.Refresh();
    }
    //protected void fillSalesRep()
    //{
    //    string sqlcommand = "SELECT USER_Initials FROM USERS";
    //    DataTable tblInits = CMethods.returnTable(sqlcommand);
    //    ddlSalesPerson.DataSource = tblInits;
    //    ddlSalesPerson.DataValueField = "USER_Initials";
    //    ddlSalesPerson.DataTextField = "USER_Initials";
    //    ddlSalesPerson.DataBind();
    //}
    protected DataTable fillQuotes(GridView gv, bool flag)
    {
        DataTable tbl;
        if (flag)
        {
            string sqlcommand = "SELECT q.*,u.USER_Initials,u.USER_ID FROM USERS u JOIN QUOTES q ON q.QUOTE_USERS_FK = u.USER_ID";
            tbl = CMethods.returnTable(sqlcommand);
        }

        else
        {
            tbl = (DataTable)Session["ReportDataTable"];
        }
        SaveTabletoSession(tbl, gv);
        //    gvProjects.DataSource = tblQuotes;
        //    gvProjects.DataBind();
        //}
        //ddlQuoteNumber.DataSource = tblQuotes;
        //ddlQuoteNumber.DataValueField = "QUOTE_ID";
        //ddlQuoteNumber.DataTextField = "QUOTE_Number";
        //ddlQuoteNumber.DataBind();
        return tbl;
    }

    private void SaveTabletoSession(DataTable tbl, GridView gv)
    {
        Session.Remove("ReportDataTable");
        Session["ReportDataTable"] = tbl;
        BindSessionTabletoGridView(gv, Session["ReportDataTable"]);

    }


    private void BindSessionTabletoGridView(GridView gv, object mySessionTable)
    {
        gv.DataSource = (DataTable)mySessionTable;
        gv.DataBind();
    }

    protected void gvProjects_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int Index = gvProjects.SelectedIndex;
        //int passedvalue = (int)gvProjects.DataKeys[Index].Value;

        //string sqlcommand = "SELECT PROJECT_Name +' '+PROJECT_NameTwo As PROJECTNAME FROM PROJECTS WHERE PROJECT_ID=@selectedid";

        //DataTable tbl = CMethods.returnTable(sqlcommand, "@selectedid", passedvalue);

        
        //txtProject.Text = tbl.Rows[0]["ProjectName"].ToString();
        //GridViewRow row = gvProjects.SelectedRow;
        //string id = row.Cells[0].Text;
    }

  
    

    protected void gvProjects_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void gvQuotes_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int Index = gvQuotes.SelectedIndex;
        //int passedvalue = (int)gvQuotes.DataKeys[Index].Value;

        ////string sqlcommand = "SELECT PROJECT_Name +' '+PROJECT_NameTwo As PROJECTNAME FROM PROJECTS WHERE PROJECT_ID=@selectedid";

        //string newcommand = "SELECT e.QUOTE_ITEMS_ItemQty, d.ITEM_PartNumber, d.ITEM_Description," + 
        //                    "d.ITEM_Cost, f.USER_Initials,e.QUOTE_ITEMS_LineTotal, (e.QUOTE_ITEMS_ItemQty*d.ITEM_Cost) AS [EXT COST] "+
        //                    "FROM QUOTE_ITEMS e JOIN ITEMS d ON e.QUOTE_ITEMS_ITEM_FK = d.ITEM_ID "+
        //                    "JOIN QUOTES g ON g.QUOTE_ID=e.QUOTE_ITEMS_QUOTE_FK "+
        //                    "JOIN USERS f ON f.USER_ID=g.QUOTE_USERS_FK "+
        //                    "WHERE g.QUOTE_PROJECT_FK=@myValue";

        //DataTable tbl = CMethods.returnTable(newcommand, "@myValue", passedvalue -1);

        //gvLineItems.DataSource = tbl;
        //gvLineItems.DataBind();
        
    }
    protected void gvProjects_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = (DataTable)Session["ReportDataTable"];
        if (dt != null)
        {
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            SaveTabletoSession(dt, gvProjects);
        }
    }
    private string GetSortDirection(string p)
    {
        string sortDirection = "ASC";
        string SortExpression = (string)ViewState["SortExpression"];
        if (SortExpression != null)
        {
            if (SortExpression == p)
            {
                string lastDirection = (string)ViewState["SortDirection"];
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = p;
        return sortDirection;

    }
    protected void gvProjects_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProjects.PageIndex = e.NewPageIndex;
        gvProjects.DataBind();
    }

    //private void DisplayCurrentPage()
    //{
    //    int currentPage = gvProjects.PageIndex + 1;
    //    gvProjects.SetPageIndex(currentPage);
    //}
    //protected void gvProjects_PageIndexChanged(object sender, EventArgs e)
    //{
    //    DisplayCurrentPage();
    //}
}