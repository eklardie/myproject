﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Items : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                Page.Validate();
                CLogin myInfo = (CLogin)Session["myLogin"];

                fillItemsGrid(GridView1,true);
                ArrayList items = new ArrayList();
                foreach (DataRow item in ((DataTable)Session["ItemsDataTable"]).AsEnumerable())
                {
                    items.Add(item["ITEM_PartNumber"].ToString());
                }

                //GridView1.DataSource = Session["ItemsDataTable"];
                //GridView1.DataBind();

            }
            //fillItemsGrid(GridView1);
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }

    }


    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        fillItemsGrid(GridView1,false);

    }

    private DataTable fillItemsGrid(GridView gv, bool flag)
    {
        DataTable tbl;
        if (flag)
        {
            

            string newCommand = "SELECT * FROM ITEMS ORDER BY ITEM_IsActive, ITEM_PartNumber";
            tbl = CMethods.returnTable(newCommand);
        }
        else
        {
           tbl = (DataTable)Session["ItemsDataTable"];
        }

        //************MOVED TO HELPER METHOD AND STORE IN SESSION VARIABLE***********
        //GridView1.DataSource = tbl;
        //GridView1.DataBind();
        SaveTabletoSession(tbl, gv);
        return tbl;
    }

    private void SaveTabletoSession(DataTable tbl, GridView gv)
    {
        Session.Remove("ItemsDataTable");        
        Session["ItemsDataTable"] = tbl;
        BindSessionTabletoGridView(gv, Session["ItemsDataTable"]);

    }


    private void BindSessionTabletoGridView(GridView gv, object mySessionTable)
    {
        gv.DataSource = (DataTable)mySessionTable;
        gv.DataBind();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;

        fillItemsGrid(GridView1,false);
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int itemid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex]["ITEM_ID"].ToString());
        TextBox txtListPrice = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtGVEditListPrice");
        TextBox txtCost = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtGVEditCost");
        CheckBox active = (CheckBox)GridView1.Rows[e.RowIndex].FindControl("CheckBox1");
        string newcommand = "UPDATE ITEMS SET ITEM_ListPrice = @listprice, ITEM_Cost = @cost, ITEM_IsActive=@active WHERE ITEM_ID = @ID";
        CMethods.executeNonQuery(newcommand, "@listprice", txtListPrice.Text, "@cost", txtCost.Text, "@active", Convert.ToBoolean(active.Checked), "@ID", itemid);
        GridView1.EditIndex = -1;
        fillItemsGrid(GridView1,true);
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = (DataTable)Session["ItemsDataTable"];
        if (dt != null)
        {
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            SaveTabletoSession(dt, GridView1);
        }
    }
    private string GetSortDirection(string p)
    {
        string sortDirection = "ASC";
        string SortExpression = (string)ViewState["SortExpression"];
        if (SortExpression != null)
        {
            if (SortExpression == p)
            {
                string lastDirection = (string)ViewState["SortDirection"];
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }
        }
        ViewState["SortDirection"] = sortDirection;
        ViewState["SortExpression"] = p;
        return sortDirection;

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string searchQuery = txtSearch.Text.Trim();
        string newCommand = "SELECT * FROM ITEMS WHERE ITEM_PartNumber LIKE @searchQuery UNION SELECT * FROM ITEMS WHERE ITEM_Description LIKE @searchQuery";
        SaveTabletoSession(CMethods.returnTable(newCommand, "@searchQuery", "%" + searchQuery + "%"), GridView1);
        txtSearch.Text = null;
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        errormessage.InnerText = null;
        string partnumber = null;
        string description = null;
        decimal listPrice;
        decimal cost;

        partnumber = TxtPartNumber.Text.Trim().ToUpper();
        description = txtDescription.Text.Trim();
        listPrice = Convert.ToDecimal(txtListPrice.Text);
        cost = Convert.ToDecimal(txtUnitCost.Text);

        string checkCommand = "SELECT * FROM ITEMS WHERE ITEM_PartNumber = @exists";
        DataTable tbl = CMethods.returnTable(checkCommand, "@exists", partnumber);
        if (tbl.Rows.Count < 1)
        {

            string newCommand = "INSERT INTO ITEMS (ITEM_PartNumber,ITEM_Description,ITEM_ListPrice,ITEM_Cost,ITEM_IsActive) VALUES (@pnum,@desc,@price,@cost,@active)";
            CMethods.executeNonQuery(newCommand, "@pnum", partnumber, "@desc", description, "@price", listPrice, "@cost", cost, "@active", 1);
            SaveTabletoSession(CMethods.returnTable("SELECT * FROM ITEMS"), GridView1);
            TxtPartNumber.Text = null;
            txtDescription.Text = null;
            txtListPrice.Text = null;
            txtUnitCost.Text = null;
        }
        else
        {
            errormessage.InnerText = "DUPLICATE PART NUMBER";
            
            TxtPartNumber.Focus();
            TxtPartNumber.Text = null;
        }
        
        
        
        
    }
}