﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Quotation : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {


        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                Page.Validate();

 //**************THIS SECTION FILLS CALENDAR YEARS**********************
                for (int i = 2010; i < 2050; i++)
                {
                    ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    ddlYear.SelectedValue = DateTime.Now.Year.ToString();
                    ddlMonth.SelectedValue = DateTime.Now.Month.ToString();
                    lbtnClearDate.Visible = true;
                }
 //************************************************************************
                CLogin myInfo = (CLogin)Session["myLogin"];
                HttpCookie _userInfoCookies = new HttpCookie("QuoteInfo");

                
                if (Request.QueryString["Quote"] != null)
                {
                    Response.Cookies.Clear();
                    string quote = Request.QueryString["Quote"].Trim();
                    _userInfoCookies["QuoteNumber"] = quote;
                    Response.Cookies.Add(_userInfoCookies);
                    fillDataTable(gvLineItems,quote,true);
                    hiddenDetails.Attributes.Remove("class");
                    hiddenDetails.Attributes.Add("class", "rightSide");
                    DataTable items = fillItemDropDown();
                    DataTable tbl = (DataTable)Session["LineItemsDataTable"];
                    if (tbl.Rows.Count > 0)
                    {
                        string holder = tbl.Rows[0][6].ToString();
                        DataTable quoteinfo = returnQuoteInfo(holder);
                        btnAddQuote.Visible = false;
                        fillStates();
                        fillSystemTypes();

                        foreach (DataRow row in quoteinfo.AsEnumerable())
                        {
                            txtSalesRep.Text = row["USER_Initials"].ToString();
                            txtDueDate.Text = ((DateTime)row["QUOTE_DueDate"]).ToShortDateString();
                            txtMarkupAmount.Text = ((decimal)row["QUOTE_MarkUp"]).ToString("C");
                            lblQuoteNumber.Text = row["QUOTE_Number"].ToString();
                            lblQuoteSubTotal.Text = ((decimal)row["QUOTE_SumOfItemsCost"]).ToString("C");
                            lblQuoteTotal.Text = ((decimal)row["QUOTE_TotalPrice"]).ToString("C");
                            txtLabor.Text = ((decimal)row["QUOTE_LaborCost"]).ToString("C");
                            txtShipping.Text = ((decimal)row["QUOTE_Shipping"]).ToString("C");
                            txtDrawings.Text = ((decimal)row["QUOTE_Drawings"]).ToString("C");
                            txtProjectName.Text = row["QUOTE_ProjectName"].ToString();
                            txtProjectNameTwo.Text = row["QUOTE_ProjectNameTwo"].ToString();
                            txtProjectAddress.Text = row["QUOTE_ProjectAddress"].ToString();
                            txtProjectAddressTwo.Text = row["QUOTE_ProjectAddressTwo"].ToString();
                            txtProjectCity.Text = row["QUOTE_City"].ToString();
                            txtProjectState.SelectedValue = row["QUOTE_State"].ToString();
                            txtProjectZip.Text = row["QUOTE_Zip"].ToString();
                            txtSystemType.SelectedValue = row["QUOTE_SystemType"].ToString();

                            txtMarkupPercent.Text = (((decimal)row["QUOTE_MarkUp"] / (decimal)row["QUOTE_SumOfItemsCost"]) * 100).ToString();
                        }
                    }
                    else
                    {
                        btnAddQuote.Visible = false;
                        fillStates();
                        fillSystemTypes();
                        DataTable quoteinfo = CMethods.returnTable("SELECT *,USER_Initials FROM QUOTES JOIN USERS ON QUOTE_USERS_FK = USER_ID WHERE QUOTE_Number = @pass", "@pass", quote);
                        quoteinfo.Columns.Add("checkmeout");

                        foreach (DataRow row in quoteinfo.AsEnumerable())
                        {
                            txtSalesRep.Text = row["USER_Initials"].ToString();
                            txtDueDate.Text = ((DateTime)row["QUOTE_DueDate"]).ToShortDateString();
                            txtMarkupAmount.Text = ((decimal)row["QUOTE_MarkUp"]).ToString("C");
                            lblQuoteNumber.Text = row["QUOTE_Number"].ToString();
                            lblQuoteSubTotal.Text = ((decimal)row["QUOTE_SumOfItemsCost"]).ToString("C");
                            lblQuoteTotal.Text = ((decimal)row["QUOTE_TotalPrice"]).ToString("C");
                            txtLabor.Text = ((decimal)row["QUOTE_LaborCost"]).ToString("C");
                            txtShipping.Text = ((decimal)row["QUOTE_Shipping"]).ToString("C");
                            txtDrawings.Text = ((decimal)row["QUOTE_Drawings"]).ToString("C");
                            txtProjectName.Text = row["QUOTE_ProjectName"].ToString();
                            txtProjectNameTwo.Text = row["QUOTE_ProjectNameTwo"].ToString();
                            txtProjectAddress.Text = row["QUOTE_ProjectAddress"].ToString();
                            txtProjectAddressTwo.Text = row["QUOTE_ProjectAddressTwo"].ToString();
                            txtProjectCity.Text = row["QUOTE_City"].ToString();
                            txtProjectState.SelectedValue = row["QUOTE_State"].ToString();
                            txtProjectZip.Text = row["QUOTE_Zip"].ToString();
                            txtSystemType.SelectedValue = row["QUOTE_SystemType"].ToString();

                            if ((decimal)row["QUOTE_sumOfItemsCost"] > 0) { txtMarkupPercent.Text = (((decimal)row["QUOTE_MarkUp"] / (decimal)row["QUOTE_SumOfItemsCost"]) * 100).ToString(); }
                            else { txtMarkupPercent.Text = "40"; }
                        }
                    }

                }
                else
                {

                string quoteNumber = generateQuoteNumber();


                txtSalesRep.Text = myInfo.Inits;
                txtDueDate.Text = DateTime.Now.AddDays(1).ToShortDateString();
                txtMarkupPercent.Text = "40";
                lblQuoteNumber.Text = quoteNumber;

                //NOT SURE IF IM USING SESSION QUOTE NUMBERS!!
                Session["quoteNumber"] = quoteNumber;

                fillStates();
                fillSystemTypes();

                //SHOULD PUT DATATABLE IN SESSION VARIABLE 

                DataTable items = fillItemDropDown();

                //IF USING SESSION VARIABLE PROBABLY DONT NEED COOKIE
                _userInfoCookies["QuoteNumber"] = quoteNumber;
                Response.Cookies.Add(_userInfoCookies);


                    fillDataTable(gvLineItems,quoteNumber,true);
                }

            }

        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    private DataTable returnQuoteInfo(string holder)
    {
        int quotenumber = Convert.ToInt32(holder);
        DataTable tbl;
        string newcommand = "SELECT q.*,u.USER_Initials FROM QUOTES q JOIN USERS u ON u.USER_ID = q.QUOTE_USERS_FK WHERE QUOTE_ID = @qnum";
        tbl = CMethods.returnTable(newcommand, "@qnum", quotenumber);
        if (tbl.Rows.Count > 0)
        {

            return tbl;
        }
        else { tbl = null; return tbl; }
    }

    private string generateQuoteNumber()
    {
        DateTime myDay = DateTime.Now;
        string year = myDay.Year.ToString();
        string prepend = year.Substring(2);
        string newcommand = "SELECT MAX(QUOTE_ID) FROM QUOTES";
        string lastquoteindex = CMethods.executeStringScalar(newcommand);
        string newQuotenumber = prepend + "1" + lastquoteindex;
        return newQuotenumber;
    }
    protected void fillStates()
    {
        txtProjectState.Items.Clear();

        DataSet stateList = new DataSet();
        stateList.ReadXml(MapPath("states.xml"));
        txtProjectState.DataSource = stateList;
        txtProjectState.DataValueField = "value";
        txtProjectState.DataTextField = "text";
        txtProjectState.DataBind();

    }
    protected void fillSystemTypes()
    {
        txtSystemType.Items.Clear();

        DataSet SystemList = new DataSet();
        SystemList.ReadXml(MapPath("SystemTypes.xml"));
        txtSystemType.DataSource = SystemList;
        txtSystemType.DataValueField = "value";
        txtSystemType.DataTextField = "text";
        txtSystemType.DataBind();

    }
    protected DataTable fillItemDropDown()
    {
        string newcommand = "SELECT * FROM ITEMS WHERE ITEM_IsActive=@1 ORDER BY ITEM_PartNumber";
        DataTable tbl = CMethods.returnTable(newcommand, "@1", 1);
        ddlPartNumber.Items.Clear();
        ddlPartNumber.DataSource = tbl;
        ddlPartNumber.DataValueField = "ITEM_ID";
        ddlPartNumber.DataTextField = "ITEM_PartNumber";
        ddlPartNumber.DataBind();
        return tbl;
    }

    private DataTable fillDataTable(GridView gv,string quote, bool flag)
    {
        DataTable tbl;
        if (flag)
        {
            string newcommand = "SELECT e.QUOTE_ITEMS_ID,e.QUOTE_ITEMS_ItemQty,d.ITEM_ID, d.ITEM_PartNumber, d.ITEM_Description, " +
                                "d.Item_Cost,e.QUOTE_ITEMS_QUOTE_FK,e.QUOTE_ITEMS_ITEM_FK, e.QUOTE_ITEMS_LineTotal, (e.QUOTE_ITEMS_ItemQty*d.ITEM_Cost) AS [EXT COST] " +
                                "FROM QUOTE_ITEMS e JOIN ITEMS d ON e.QUOTE_ITEMS_ITEM_FK = d.ITEM_ID " +
                                "JOIN QUOTES g ON g.QUOTE_ID=e.QUOTE_ITEMS_QUOTE_FK " +
                                "WHERE g.QUOTE_Number=@myValue";
            tbl = CMethods.returnTable(newcommand, "@myValue", Convert.ToInt32(quote));
        }
        else
        {
            tbl = (DataTable)Session["LineItemsDataTable"];
        }

        SaveTableToSession(tbl, gv);
        return tbl;
    }

    private void SaveTableToSession(DataTable tbl, GridView gv) 
    {
        Session.Remove("LineItemsDataTable");
        Session["LineItemsDataTable"] = tbl;
        BindSessionTabletoGridView(gv, Session["LineItemsDataTable"]);
    }

    private void BindSessionTabletoGridView(GridView gv, object tbl)
    {
        gv.DataSource = (DataTable)tbl;
        gv.DataBind();
    }

    //protected void fillLineItems(string quotenumber)
    //{
    //    string newcommand = "SELECT e.QUOTE_ITEMS_ItemQty, d.ITEM_PartNumber, d.ITEM_Description," +
    //                        "d.ITEM_Cost, e.QUOTE_ITEMS_LineTotal, (e.QUOTE_ITEMS_ItemQty*d.ITEM_Cost) AS [EXT COST] " +
    //                        "FROM QUOTE_ITEMS e JOIN ITEMS d ON e.QUOTE_ITEMS_ITEM_FK = d.ITEM_ID " +
    //                        "JOIN QUOTES g ON g.QUOTE_ID=e.QUOTE_ITEMS_QUOTE_FK " +
    //                        "WHERE g.QUOTE_Number=@myValue";

    //    DataTable tbl = CMethods.returnTable(newcommand, "@myValue", Convert.ToInt32(quotenumber));

    //    gvLineItems.DataSource = tbl;
    //    gvLineItems.DataBind();
    //}

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectCalendar();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectCalendar();
    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        PopupControlExtender1.Commit(Calendar1.SelectedDate.ToString("d"));
    }
    protected void SelectCalendar()
    {
        if (ddlYear.SelectedValue != "")
        {

            Calendar1.VisibleDate = DateTime.Parse(ddlMonth.SelectedValue + "/" + DateTime.Now.Day + "/" + ddlYear.SelectedValue);
        }
    }
    protected void lbtnToday_Click(object sender, EventArgs e)
    {
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();
        ddlMonth.SelectedValue = DateTime.Now.Month.ToString();
        Calendar1.SelectedDate = DateTime.Now;
        PopupControlExtender1.Commit(Calendar1.SelectedDate.ToString("d"));
    }
    protected void lbtnClearDate_Click(object sender, EventArgs e)
    {
        txtDueDate.Text = "";
        PopupControlExtender1.Commit("");
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        HttpCookie _userInfoCookies = Request.Cookies["QuoteInfo"];
        string cookieQuoteNumber = "";
        if (_userInfoCookies != null)
        {
            cookieQuoteNumber = _userInfoCookies["QuoteNumber"];
            decimal extcost = Convert.ToDecimal(decimal.Parse(lblLineTotal.Text, NumberStyles.Currency));
            string newcommand = "INSERT INTO QUOTE_ITEMS (QUOTE_ITEMS_QUOTE_FK,QUOTE_ITEMS_ITEM_FK,QUOTE_ITEMS_ItemQty,QUOTE_ITEMS_LineTotal) VALUES ((SELECT QUOTE_ID FROM QUOTES WHERE QUOTE_Number = @quotenum),@itemnum,@itemqty,@extcost)";
            CMethods.executeNonQuery(newcommand, "@quotenum", Convert.ToInt32(cookieQuoteNumber), "@itemnum", Convert.ToInt32(ddlPartNumber.SelectedValue), "@itemqty", Convert.ToInt32(txtQuantity.Text), "@extcost", extcost);
            //QDetails details = (QDetails)Session["thisQuote
            string secondCommand = "SELECT SUM(QUOTE_ITEMS_LineTotal) FROM QUOTE_ITEMS WHERE QUOTE_ITEMS_QUOTE_FK = (SELECT QUOTE_ID FROM QUOTES WHERE QUOTE_Number = @qnum)";
            decimal subtotal = CMethods.executeDecimalScalar(secondCommand, "@qnum", Convert.ToInt32(cookieQuoteNumber));
            lblQuoteSubTotal.Text = subtotal.ToString("C");
            decimal laborCost = 100;
            if ((decimal)((float)decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) * .1) < 100)
            {
                laborCost = 100;
            }
            else
            {
                laborCost = (decimal)((float)decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) * .1);
            }
            decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
            decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
            decimal shipping = 100M;
            if ((float)subtotal * 0.02 < 100)
            {
                shipping = 100M;
            }
            else
            {
                shipping = (decimal)((float)subtotal * 0.02);
            }
            decimal drawings = decimal.Parse(txtDrawings.Text,NumberStyles.Currency);
            //decimal totalCost = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) + Convert.ToDecimal(txtDrawings.Text) + laborCost + markup + shipping;
            txtLabor.Text = laborCost.ToString("C");
            txtMarkupAmount.Text = markup.ToString("C");
            txtShipping.Text = shipping.ToString("C");
            calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);
            fillDataTable(gvLineItems,cookieQuoteNumber,true);
        }
        else
        {
            gvLineItems.DataSource = null;
            gvLineItems.EmptyDataText = "YOU MUST FIRST CREATE A PROJECT FOR THIS QUOTATION";
            gvLineItems.DataBind();
        }
    }

    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        string newcommand = "SELECT ITEM_Cost FROM ITEMS WHERE ITEM_ID=@itemid";
        decimal cost = CMethods.executeDecimalScalar(newcommand, "@itemid", ddlPartNumber.SelectedValue);
        decimal extCost = Convert.ToDecimal(txtQuantity.Text) * cost;
        lblLineTotal.Text = extCost.ToString("C");
    }
    protected void btnAddQuote_Click(object sender, EventArgs e)
    {
        DateTime quotedue = Convert.ToDateTime(txtDueDate.Text);
        HttpCookie _userInfoCookies = Request.Cookies["QuoteInfo"];
        string cookieQuoteNumber = "";
        if (_userInfoCookies != null)
        {
            cookieQuoteNumber = _userInfoCookies["QuoteNumber"];
        }
        else
        {
            cookieQuoteNumber = generateQuoteNumber();
            _userInfoCookies["QuoteNumber"] = cookieQuoteNumber;
            Response.Cookies.Add(_userInfoCookies);
        }
        Guid userid = ((CLogin)Session["myLogin"]).ID;
        string uid = CMethods.executeStringScalar("SELECT USER_ID FROM USERS WHERE USER_UID = @uid","@uid",userid);
        int passableid = Convert.ToInt32(uid);


        string newCommand = "INSERT INTO QUOTES (QUOTE_Number, QUOTE_SystemType, QUOTE_DueDate, QUOTE_ProjectName, QUOTE_ProjectNameTwo, QUOTE_ProjectAddress, QUOTE_ProjectAddressTwo, QUOTE_City, QUOTE_State, QUOTE_Zip,QUOTE_USERS_FK) " +
                            "VALUES (@qnumber, @qsystype, @qdue, @qpname, @qpname2, @qpadd, @qpadd2, @qpcity, @qpstate, @qpzip,@uid)";
        CMethods.executeNonQuery(newCommand, "@qnumber", Convert.ToInt32(cookieQuoteNumber), "@qsystype", txtSystemType.Text, "@qdue", quotedue, "@qpname", txtProjectName.Text, "@qpname2", txtProjectNameTwo.Text, "@qpadd", txtProjectAddress.Text, "@qpadd2", txtProjectAddressTwo.Text, "@qpcity", txtProjectCity.Text, "@qpstate", txtProjectState.Text, "@qpzip", txtProjectZip.Text, "@uid",passableid);

        hiddenDetails.Attributes.Remove("class");
        hiddenDetails.Attributes.Add("class", "rightSide");
    }

    private void calculateTotalCost(decimal _subtotal, decimal _labor, decimal _drawings, decimal _markup, decimal _shipping)
    {
        decimal subtotal = _subtotal;
        decimal laborCost = _labor;
        decimal drawings = _drawings;
        decimal markup = _markup;
        decimal shipping = _shipping;

        decimal totalcost = subtotal + laborCost + drawings + markup + shipping;
        lblQuoteTotal.Text = totalcost.ToString("C");

    }

    protected void txtMarkupPercent_TextChanged(object sender, EventArgs e)
    {
        decimal subtotal = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency);
        decimal laborCost = decimal.Parse(txtLabor.Text, NumberStyles.Currency);
        decimal drawings = decimal.Parse(txtDrawings.Text, NumberStyles.Currency);
        decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
        decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
        decimal shipping = decimal.Parse(txtShipping.Text, NumberStyles.Currency);

        txtMarkupAmount.Text = markup.ToString("C");
        calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);

    }
    protected void txtDrawings_TextChanged(object sender, EventArgs e)
    {
        decimal subtotal = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency);
        decimal laborCost = decimal.Parse(txtLabor.Text, NumberStyles.Currency);
        decimal drawings = Convert.ToDecimal(txtDrawings.Text);
        decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
        decimal markup = decimal.Parse(txtMarkupAmount.Text, NumberStyles.Currency);
        //decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
        decimal shipping = decimal.Parse(txtShipping.Text, NumberStyles.Currency);

        txtDrawings.Text = drawings.ToString("C");
        calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);

    }
    protected void txtLabor_TextChanged(object sender, EventArgs e)
    {
        decimal subtotal = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency);
        decimal drawings = decimal.Parse(txtDrawings.Text, NumberStyles.Currency);
        decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
        //decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
        decimal markup = decimal.Parse(txtMarkupAmount.Text, NumberStyles.Currency);
        decimal laborCost = decimal.Parse(txtLabor.Text, NumberStyles.Currency);
        decimal shipping = decimal.Parse(txtShipping.Text, NumberStyles.Currency);

        txtLabor.Text = laborCost.ToString("C");
        calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);
    }
    protected void txtShipping_TextChanged(object sender, EventArgs e)
    {
        decimal subtotal = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency);
        decimal drawings = decimal.Parse(txtDrawings.Text, NumberStyles.Currency);
        decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
        //decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
        decimal markup = decimal.Parse(txtMarkupAmount.Text, NumberStyles.Currency);
        decimal laborCost = decimal.Parse(txtLabor.Text, NumberStyles.Currency);
        decimal shipping = decimal.Parse(txtShipping.Text, NumberStyles.Currency);

        txtShipping.Text = shipping.ToString("C");
        calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);
    }
    protected void btnFinalize_Click(object sender, EventArgs e)
    {
        HttpCookie _userInfoCookies = Request.Cookies["QuoteInfo"];
        string cookieQuoteNumber = "";
        if (_userInfoCookies != null)
        {
            cookieQuoteNumber = _userInfoCookies["QuoteNumber"];



            string newCommand = "UPDATE QUOTES SET QUOTE_SumOfItemsCost = @subtotal, " +
                                "QUOTE_LaborCost = @labor, " +
                                "QUOTE_MarkUp = @markup, " +
                                "QUOTE_Drawings = @drawings, " +
                                "QUOTE_Shipping = @shipping, " +
                                "QUOTE_TotalPrice = @totalprice " +
                                "WHERE QUOTE_Number = @qnum";
            int quoteID = Convert.ToInt32(cookieQuoteNumber);
            decimal subtotal = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency);
            decimal drawings = decimal.Parse(txtDrawings.Text, NumberStyles.Currency);
            decimal markup = decimal.Parse(txtMarkupAmount.Text, NumberStyles.Currency);
            decimal laborCost = decimal.Parse(txtLabor.Text, NumberStyles.Currency);
            decimal shipping = decimal.Parse(txtShipping.Text, NumberStyles.Currency);
            decimal quoteTotal = decimal.Parse(lblQuoteTotal.Text, NumberStyles.Currency);

            CLogin myLogin = (CLogin)Session["myLogin"];

            CMethods.executeNonQuery(newCommand, "@subtotal", subtotal, "@labor", laborCost, "@markup", markup, "@drawings", drawings, "@shipping", shipping, "@totalprice", quoteTotal, "@qnum", quoteID);
            Response.Redirect("QuotePresentation.aspx?usr="+myLogin.ID+"&admin="+myLogin.Admin + "&Quote="+cookieQuoteNumber);
        }
    }
    protected void gvLineItems_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvLineItems.EditIndex = e.NewEditIndex;
        fillDataTable(gvLineItems, Request.Cookies["QuoteInfo"]["QuoteNumber"], false);
    }
    protected void gvLineItems_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvLineItems_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int lineitemid = Convert.ToInt32(gvLineItems.DataKeys[e.RowIndex]["QUOTE_ITEMS_ID"].ToString());
        TextBox txtQuantity = (TextBox)gvLineItems.Rows[e.RowIndex].FindControl("txtGVEditQTY");
        string newCommand = "UPDATE QUOTE_ITEMS SET QUOTE_ITEMS_ItemQty = @qty, QUOTE_ITEMS_LineTotal = ((SELECT ITEM_Cost FROM ITEMS WHERE ITEM_ID = @itemid)*@quantity)  WHERE QUOTE_ITEMS_ID = @qiid";
        CMethods.executeNonQuery(newCommand, "@qty", Convert.ToInt32(txtQuantity.Text),"@itemid",Convert.ToInt32(gvLineItems.DataKeys[e.RowIndex]["ITEM_ID"]),"@quantity",Convert.ToInt32(txtQuantity.Text), "@qiid", lineitemid);
        gvLineItems.EditIndex = -1;
        fillDataTable(gvLineItems, Request.Cookies["QuoteInfo"]["QuoteNumber"], true);
        fieldUpdates();

    }
    protected void gvLineItems_Sorting(object sender, GridViewSortEventArgs e)
    {

    }
    protected void gvLineItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int lineitemid = Convert.ToInt32(gvLineItems.DataKeys[e.RowIndex]["QUOTE_ITEMS_ID"].ToString());
        string newCommand = "DELETE FROM QUOTE_ITEMS WHERE QUOTE_ITEMS_ID = @qiid";
        CMethods.executeNonQuery(newCommand, "@qiid", lineitemid);
        
        DataTable temptbl = fillDataTable(gvLineItems, Request.Cookies["QuoteInfo"]["QuoteNumber"], true);
        if (temptbl.Rows.Count > 0)
        {
            fieldUpdates();
        }
        else
        {
            decimal subtotal = 0;
            lblQuoteSubTotal.Text = subtotal.ToString("C");
            decimal laborCost = 100;
            if ((decimal)((float)decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) * .1) < 100)
            {
                laborCost = 100;
            }
            else
            {
                laborCost = (decimal)((float)decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) * .1);
            }
            decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
            decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
            decimal shipping = 100M;
            if ((float)subtotal * 0.02 < 100)
            {
                shipping = 100M;
            }
            else
            {
                shipping = (decimal)((float)subtotal * 0.02);
            }
            decimal drawings = decimal.Parse(txtDrawings.Text, NumberStyles.Currency);
            //decimal totalCost = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) + Convert.ToDecimal(txtDrawings.Text) + laborCost + markup + shipping;
            txtLabor.Text = laborCost.ToString("C");
            txtMarkupAmount.Text = markup.ToString("C");
            txtShipping.Text = shipping.ToString("C");
            calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);
        }
        string anothernewCommand = "UPDATE QUOTES SET QUOTE_SumOfItemsCost = @subtotal, " +
                               "QUOTE_LaborCost = @labor, " +
                               "QUOTE_MarkUp = @markup, " +
                               "QUOTE_Drawings = @drawings, " +
                               "QUOTE_Shipping = @shipping, " +
                               "QUOTE_TotalPrice = @totalprice " +
                               "WHERE QUOTE_Number = @qnum";
        decimal anothersubtotal = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency);
        decimal anotherdrawings = decimal.Parse(txtDrawings.Text, NumberStyles.Currency);
        decimal anothermarkup = decimal.Parse(txtMarkupAmount.Text, NumberStyles.Currency);
        decimal anotherlaborCost = decimal.Parse(txtLabor.Text, NumberStyles.Currency);
        decimal anothershipping = decimal.Parse(txtShipping.Text, NumberStyles.Currency);
        decimal anotherquoteTotal = decimal.Parse(lblQuoteTotal.Text, NumberStyles.Currency);

        CLogin myLogin = (CLogin)Session["myLogin"];

        CMethods.executeNonQuery(anothernewCommand, "@subtotal", anothersubtotal, "@labor", anotherlaborCost, "@markup", anothermarkup, "@drawings", anotherdrawings, "@shipping", anothershipping, "@totalprice", anotherquoteTotal, "@qnum", Convert.ToInt32(Request.QueryString["Quote"].Trim()));
        Response.Cookies.Clear();
        string quote = Request.QueryString["Quote"].Trim();
        HttpCookie _userInfoCookies = new HttpCookie("QuoteInfo");
        _userInfoCookies["QuoteNumber"] = quote;
        Response.Cookies.Add(_userInfoCookies);

    }
    protected void gvLineItems_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvLineItems.EditIndex = -1;
        fillDataTable(gvLineItems, Request.Cookies["QuoteInfo"]["QuoteNumber"], false);
    }
    private void fieldUpdates()
    {
        string cookieQuoteNumber = Request.Cookies["QuoteInfo"]["QuoteNumber"];
        string secondCommand = "SELECT SUM(QUOTE_ITEMS_LineTotal) FROM QUOTE_ITEMS WHERE QUOTE_ITEMS_QUOTE_FK = (SELECT QUOTE_ID FROM QUOTES WHERE QUOTE_Number = @qnum)";
        decimal subtotal = CMethods.executeDecimalScalar(secondCommand, "@qnum", Convert.ToInt32(cookieQuoteNumber));
        lblQuoteSubTotal.Text = subtotal.ToString("C");
        decimal laborCost = 100;
        if ((decimal)((float)decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) * .1) < 100)
        {
            laborCost = 100;
        }
        else
        {
            laborCost = (decimal)((float)decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) * .1);
        }
        decimal marginPercentage = (decimal)((float.Parse(txtMarkupPercent.Text) / 100));
        decimal markup = (decimal)((float)marginPercentage * (float)subtotal);
        decimal shipping = 100M;
        if ((float)subtotal * 0.02 < 100)
        {
            shipping = 100M;
        }
        else
        {
            shipping = (decimal)((float)subtotal * 0.02);
        }
        decimal drawings = decimal.Parse(txtDrawings.Text,NumberStyles.Currency);
        //decimal totalCost = decimal.Parse(lblQuoteSubTotal.Text, NumberStyles.Currency) + Convert.ToDecimal(txtDrawings.Text) + laborCost + markup + shipping;
        txtLabor.Text = laborCost.ToString("C");
        txtMarkupAmount.Text = markup.ToString("C");
        txtShipping.Text = shipping.ToString("C");
        calculateTotalCost(subtotal, laborCost, drawings, markup, shipping);
    }
}