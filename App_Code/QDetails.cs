﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for QDetails
/// </summary>
public class QDetails
{

    public QDetails(int QuoteID, int QuoteNumber, decimal Drawings, decimal Markup, decimal Labor, decimal Shipping)
    {
        _qid = QuoteID;
        _qnum = QuoteNumber;
        _draw = Drawings;
        _mark = Markup;
        _labor = Labor;
        _ship = Shipping;
    }

    private int _qid;
    private int _qnum;
    private decimal _draw;
    private decimal _mark;
    private decimal _labor;
    private decimal _ship;

    public int QuoteID
    {
        get { return _qid; }
    }

    public int QuoteNumber
    {
        get { return _qnum; }
    }

    public decimal Drawings
    {
        get { return _draw; }
    }

    public decimal Markup
    {
        get { return _mark; }
    }

    public decimal Labor
    {
        get { return _labor; }
    }

    public decimal Shipping
    {
        get { return _ship; }
    }
}