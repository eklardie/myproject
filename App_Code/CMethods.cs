﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Web.UI;


/// <summary>
/// Summary description for CMethods
/// </summary>
public class CMethods
{
    public static Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn =
                FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }

    public static DataTable returnTable(String CommandText, params Object[] values)
    {
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand(CommandText, con);
        for (int i = 0; i < values.Length; i += 2)
        {
            cmd.Parameters.AddWithValue((String)values[i], values[i + 1]);
        }
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds, "tbl");
        return ds.Tables["tbl"];
    }

    public static bool executeNonQuery(String CommandText, params Object[] values)
    {
        bool bln = true;
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand(CommandText, con);
        for (int i = 0; i < values.Length; i += 2)
        {
            cmd.Parameters.AddWithValue((String)values[i], values[i + 1]);
        }
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            bln = false;
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }
        return bln;
    }

    public static int executeScalar(String CommandText, params Object[] values)
    {
        int scalar = 0;
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand(CommandText, con);
        for (int i = 0; i < values.Length; i += 2)
        {
            cmd.Parameters.AddWithValue((String)values[i], values[i + 1]);
        }
        try
        {
            con.Open();
            scalar = Convert.ToInt16(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }
        return scalar;
    }
    public static decimal executeDecimalScalar(String CommandText, params Object[] values)
    {
        decimal scalar = 0;
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand(CommandText, con);
        for (int i = 0; i < values.Length; i += 2)
        {
            cmd.Parameters.AddWithValue((String)values[i], values[i + 1]);
        }
        try
        {
            con.Open();
            scalar = Convert.ToDecimal(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }
        return scalar;
    }

    public static String executeStringScalar(String CommandText, params Object[] values)
    {
        String scalar = String.Empty;
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand(CommandText, con);
        for (int i = 0; i < values.Length; i += 2)
        {
            cmd.Parameters.AddWithValue((String)values[i], values[i + 1]);
        }
        try
        {
            con.Open();
            scalar = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }
        return scalar;
    }
    public static void recordError(string errorMessage, string page, DateTime d)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "INSERT INTO tblERRORS (errorMessage, page, date) VALUES (@error, @page, @date)";
        cmd.Parameters.AddWithValue("@error", errorMessage);
        cmd.Parameters.AddWithValue("@page", page);
        cmd.Parameters.AddWithValue("@date", d);

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }
    }

    public static String getHTTP()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "SELECT pgsHTTP FROM tblPAGES WHERE pgsDefault=1";
        String strHTTP = String.Empty;
        try
        {
            con.Open();
            strHTTP = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }

        return strHTTP;
    }

    public static String getHTTP(String id)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "SELECT pgsHTTP FROM tblPAGES WHERE pgs_id=@id AND pgsInactive=0";
        cmd.Parameters.AddWithValue("@id", new Guid(id));
        String strHTTP = String.Empty;
        try
        {
            con.Open();
            strHTTP = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
            strHTTP = "This page is no longer active.";
        }
        finally
        {
            con.Close();
        }
        
        return strHTTP;
    }

    public static String getHTTPPath(String path)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = "SELECT pgsHTTP FROM tblPAGES WHERE pgsPath LIKE @path";
        cmd.Parameters.AddWithValue("@path", path);
        String strHTTP = String.Empty;
        try
        {
            con.Open();
            strHTTP = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            con.Close();
        }

        return strHTTP;
    }

    private static int returnNumSlashes(String path)
    {
        int cnt = 0;
        foreach (char slash in path)
        {
            if (slash == '/')
            {
                cnt++;
            }
        }
        return cnt;
    }

    public static int returnFinalRank(String path, int currentRank)
    {
        int currentNumSlashes = returnNumSlashes(path);
        String str = "SELECT * FROM tblPAGES WHERE pgsRank > @currentRank ORDER BY pgsRank";
        DataTable dt = returnTable(str, "@currentRank", currentRank);
        int finalRank = 0;
        foreach (DataRow dr in dt.Rows)
        {
            if (returnNumSlashes(dr["pgsPath"].ToString()) <= currentNumSlashes)
            {
                finalRank = Convert.ToInt16(dr["pgsRank"]);
                //increase all ranks below
                String strIncrease = String.Empty;
                strIncrease = "UPDATE tblPAGES SET pgsRank=pgsRank+1 WHERE pgsRank>=@finalRank";
                CMethods.executeNonQuery(strIncrease, "@finalRank", finalRank);
                break;
            }
        }
        if (finalRank == 0)
        {
            finalRank = executeScalar("select MAX(pgsRank) FROM tblPAGES") + 1;
        }
        return finalRank;
    }

    public static Boolean duplicatePathExists(String path)
    {
        String str = "SELECT count(*) FROM tblPAGES " +
                     "WHERE pgsPath LIKE @path";
        int cnt = executeScalar(str, "@path", path);
        if (cnt == 0)
            return false;
        else
            return true;
    }

    public static void rerank()
    {
        int cnt = 0;
        DataTable dt = returnTable("SELECT pgs_id FROM tblPAGES ORDER By pgsRank");
        foreach (DataRow dr in dt.Rows)
        {
            cnt++;
            executeNonQuery("UPDATE tblPAGES SET pgsRank=@rank WHERE pgs_id=@id", "@rank", cnt, "@id", (Guid)dr["pgs_id"]);
        }
    }

    public static int returnRankAbove(int rank)
    {
        //given the rank of page you want to move up within
        //level returns the new rank.  returns zero if you can't move up
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT * FROM tblPAGES ORDER BY pgsRank DESC";
        cmd.Connection = con;
        int newRank = 0;
        Boolean FoundRank = false;
        String foundPagepath = String.Empty;
        SqlDataReader results = null;
        try
        {
            con.Open();
            results = cmd.ExecuteReader();
            while (results.Read())
            {
                if (FoundRank)
                {
                    if (results["pgsPath"].ToString().Contains("/"))
                    {
                        if (foundPagepath == results["pgsPath"].ToString().Remove
                        (results["pgsPath"].ToString().LastIndexOf("/")))
                        {
                            newRank = (int)results["pgsRank"];
                            break;
                        }
                    }
                    else
                    {
                        if (foundPagepath == results["pgsPath"].ToString())
                        {
                            newRank = 0;// (int)results["pgsRank"];
                            break;
                        }
                    }
                }


                if ((int)results["pgsRank"] == rank)
                {
                    FoundRank = true;
                    if (results["pgsPath"].ToString().Contains("/"))
                    {
                        foundPagepath = results["pgsPath"].ToString().Remove
                            (results["pgsPath"].ToString().LastIndexOf("/"));
                    }
                    else
                    {
                        foundPagepath = results["pgsPath"].ToString();
                    }
                }

            }
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            results.Close();
            con.Close();
        }
        return newRank;
    }

    public static int returnRankBelow(int rank)
    {
        //given the rank of page you want to move down within
        //level returns the new rank.  returns zero if you can't move up
        SqlConnection con =
        new SqlConnection(ConfigurationManager.ConnectionStrings["Provider"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT * FROM tblPAGES ORDER BY pgsRank";
        cmd.Connection = con;
        int newRank = 0;
        Boolean FoundRank = false;
        String foundPagepath = String.Empty;
        SqlDataReader results = null;
        try
        {
            con.Open();
            results = cmd.ExecuteReader();
            while (results.Read())
            {
                if (FoundRank)
                {
                    if (results["pgsPath"].ToString().Contains("/"))
                    {
                        if (foundPagepath == results["pgsPath"].ToString().Remove
                        (results["pgsPath"].ToString().LastIndexOf("/")))
                        {
                            newRank = (int)results["pgsRank"];
                            break;
                        }
                    }
                    else
                    {
                        if (foundPagepath == results["pgsPath"].ToString())
                        {
                            newRank = 0;// (int)results["pgsRank"];
                            break;
                        }
                    }
                }


                if ((int)results["pgsRank"] == rank)
                {
                    FoundRank = true;
                    if (results["pgsPath"].ToString().Contains("/"))
                    {
                        foundPagepath = results["pgsPath"].ToString().Remove
                            (results["pgsPath"].ToString().LastIndexOf("/"));
                    }
                    else
                    {
                        foundPagepath = results["pgsPath"].ToString();
                    }
                }

            }
        }
        catch (Exception ex)
        {
            recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        finally
        {
            results.Close();
            con.Close();
        }
        return newRank;
    }

    public static string returnOnlyNumbers(string phone)
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in phone.ToCharArray())
        {
            if (char.IsDigit(c))
                sb.Append(c);
        }
        return sb.ToString();
    }

    public static string getInterval(int Interval)
    {
        switch (Interval)
        {
            case 1:
                return "Daily";
            case 2:
                return "Weekly";
            case 3:
                return "Monthly";
            case 4:
                return "Quarterly";
            default:
                return "error";
        }
    }
}