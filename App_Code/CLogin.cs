﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CLogin
/// </summary>
public class CLogin
{
	public CLogin(string FirstName, string LastName, string Inits, string Email, Guid ID, bool Admin)
	{
        _fname = FirstName;
        _lname = LastName;
        _inits = Inits;
        _email = Email;
        _id = ID;
        _admin = Admin;
	}

    private string _fname;
    private string _lname;
    private string _inits;
    private string _email;
    private Guid _id;
    private bool _admin;

    public string FirstName
    {
        get { return _fname; }
    }

    public string LastName
    {
        get { return _lname; }
    }

    public string Inits
    {
        get { return _inits; }
    }

    public string Email
    {
        get { return _email; }
    }

    public Guid ID
    {
        get { return _id; }
    }

    public bool Admin
    {
        get { return _admin; }
    }

}