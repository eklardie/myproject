﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

/// <summary>
/// Summary description for CEmail
/// </summary>
public class CEmail
{
	public CEmail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static bool sendEmail(string From, string To, string Subject, string Body)
    {
        bool blnSent = true;
        MailMessage mm = new MailMessage(From, To, Subject, Body);
        mm.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        System.Net.NetworkCredential aCred = new System.Net.NetworkCredential("webdnhti240@gmail.com", "AdminIT240");
        smtp.EnableSsl = true;
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = aCred;
        try
        {
            smtp.Send(mm);
        }
        catch (Exception ex)
        {
            blnSent = false;
            CMethods.recordError(ex.Message, HttpContext.Current.Request.ServerVariables["URL"].ToString(), DateTime.Now);
        }
        return blnSent;
    }
}