﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="QuotePresentation.aspx.cs" Inherits="QuotePresentation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div id="quoteContainer" style="width:80%;height:auto;clear:both;">
        <div id="quoteheadersection">
            
            <div id="quoteprojectinfo" style="float:left;width:300px;margin-left:150px;">
                <br />
                <br />
                <br />
                <br />
                <br />
                <span class="insetthis">Project Name:</span><asp:Label ID="lblprojectname" runat="server" Text="Label"></asp:Label><br />
                <span class="insetthis">&nbsp;</span><asp:Label ID="lblprojectnametwo" runat="server" Text="Label"></asp:Label><br />
                <span class="insetthis">Project Address:</span><asp:Label ID="lblprojectaddress" runat="server" Text="Label"></asp:Label><br />
                <span class="insetthis">&nbsp;</span><asp:Label ID="lblprojectaddresstwo" runat="server" Text="Label"></asp:Label><br />
                <span class="insetthis">&nbsp;</span><asp:Label ID="lblcity" runat="server" Text="Label"></asp:Label>
                <span class="insetthis">&nbsp;</span><asp:Label ID="lblstate" runat="server" Text="Label"></asp:Label><br />
                <span class="insetthis">&nbsp;</span>&nbsp;<asp:Label ID="lblzip" runat="server" Text="Label"></asp:Label>
                
                
            </div>
            <div id="quoteofficeinfo" style="float:left;width:300px;">
                <span id="logoblock"><a href="#"><img src="/images/bklogo.jpg" /></a></span><br /><br />
                <span style="padding-left:100px;">Quote #:&nbsp;&nbsp;</span><asp:Label ID="lblquotenumber" runat="server" Text="Label"></asp:Label><br />
                <span style="padding-left:100px;">SalesPerson:&nbsp;&nbsp;</span><asp:Label ID="lblsalesperson" runat="server" Text="Label"></asp:Label><br />
                <span style="padding-left:100px;">Quote Date:&nbsp;&nbsp;</span><asp:Label ID="lblquotedate" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
        <div id="lineitemsection" style="clear:both;margin-left:150px;overflow:visible;padding-top:20px;">
            <span style="padding-left:200px;font-size:16px;font-weight:bold;"><asp:Label ID="lblsystemtype" runat="server" Text="Label"></asp:Label></span><br /><br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="800">
                <Columns>
                    <asp:TemplateField HeaderText="QTY">
                        <ItemTemplate><%# Eval("QUOTE_ITEMS_ItemQty") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Part Number">
                        <ItemTemplate><%# Eval("ITEM_PartNumber") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate><%# Eval("ITEM_Description") %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="quotefootersection" style="clear:both;width:80%;padding-top:10px;">
            <div id="footernotessection" style="float:left;width:300px;margin-left:150px;font-size:12px;">
                <span>This Quote is Valid For 30 Days.</span><br /><br />
                <span>Quote Price Reflects Item Quantities as Indicated.</span><br /><br />
                <span>Special Extras are Not included in Quoted Price.</span><br /><br />
                <span>Quoted Price Includes Test with the Authority Having Jurisdiction.</span><br /><br />
                <span>&nbsp;</span>
                <span>Thank you</span>
            </div>
            <div id="footersubtotalsection" style="float:left;margin-left:50px;">
                <span><strong>TOTAL COST:&nbsp;&nbsp;</strong></span><strong><asp:Label ID="lblquotetotal" runat="server" Text="Label"></asp:Label></strong>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" Runat="Server">
</asp:Content>

