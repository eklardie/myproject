﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Quotation.aspx.cs" Inherits="Quotation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <div class="pageContainer">
        <div class="topSpacing"><span class="colorMe">""</span></div>
        <div class="leftMargin">
            <span></span>
        </div>
        <div class="leftSide">
            <span class="infoLabels">
                <br />
            </span>
            <span class="infoLabels">CREATE A NEW PROJECT</span><span>QUOTATION #  </span>
            <asp:Label ID="lblQuoteNumber" runat="server" Text=""></asp:Label>
            <div class="projectCreate">
                <div class="addSpace">
                    <span class="labelColumn">Project Name:</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* " ControlToValidate="txtProjectName" ForeColor="Red" Font-Bold="true" Font-Size="9pt"></asp:RequiredFieldValidator><asp:TextBox ID="txtProjectName" runat="server"></asp:TextBox>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Project Additional Info:</span>
                    <span style="color: white; font-weight: bold; font-size: 9pt;">*&nbsp;</span><asp:TextBox ID="txtProjectNameTwo" runat="server"></asp:TextBox>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Project Address:</span>
                    <span style="color: white; font-weight: bold; font-size: 9pt;">*&nbsp;</span><asp:TextBox ID="txtProjectAddress" runat="server"></asp:TextBox>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Project Additional Address:</span>
                    <span style="color: white; font-weight: bold; font-size: 9pt;">*&nbsp;</span><asp:TextBox ID="txtProjectAddressTwo" runat="server"></asp:TextBox>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Project City:</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* " ControlToValidate="txtProjectCity" ForeColor="Red" Font-Bold="true" Font-Size="9pt"></asp:RequiredFieldValidator><asp:TextBox ID="txtProjectCity" runat="server"></asp:TextBox>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Project State:</span>
                    <span style="color: white; font-weight: bold; font-size: 9pt;">*&nbsp;</span><asp:DropDownList ID="txtProjectState" runat="server"></asp:DropDownList>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Project Zip Code:</span>
                    <span style="color: white; font-weight: bold; font-size: 9pt;">*&nbsp;</span><asp:TextBox ID="txtProjectZip" runat="server"></asp:TextBox>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Bid Date:</span>
                    <span style="color: red; font-weight: bold; font-size: 9pt;">*&nbsp;</span><asp:TextBox ID="txtDueDate" runat="server"></asp:TextBox><br />
                    <asp:Panel ID="Panel1" runat="server" CssClass="popupControl" BorderStyle="Solid" BackColor="AliceBlue" BorderColor="DarkBlue" BorderWidth="1">
                        <asp:UpdatePanel runat="server" ID="up1">
                            <ContentTemplate>
                                <center>
                                <asp:DropDownList ID="ddlMonth" runat="server" onSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true" Width="90">
                                    <asp:ListItem Value="1">January</asp:ListItem>
                                    <asp:ListItem Value="2">February</asp:ListItem>
                                    <asp:ListItem Value="3">March</asp:ListItem>
                                    <asp:ListItem Value="4">April</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">June</asp:ListItem>
                                    <asp:ListItem Value="7">July</asp:ListItem>
                                    <asp:ListItem Value="8">August</asp:ListItem>
                                    <asp:ListItem Value="9">September</asp:ListItem>
                                    <asp:ListItem Value="10">October</asp:ListItem>
                                    <asp:ListItem Value="11">November</asp:ListItem>
                                    <asp:ListItem Value="12">December</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="True" Width="60"></asp:DropDownList>
                                <asp:Calendar ID="Calendar1" runat="server" Width="150px" DayNameFormat="Shortest" BackColor="White" BorderColor="#3366CC" CellPadding="0" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" 
                                    OnSelectionChanged="Calendar1_SelectionChanged" ShowNextPrevMonth="False" ShowTitle="False" BorderWidth="1px" FirstDayOfWeek="Sunday" Height="150px" 
                                    SelectedDate="2007-08-08">
                                    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" /> 
                                    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" /> 
                                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" /> 
                                    <WeekendDayStyle BackColor="#CCCCFF" /> 
                                    <OtherMonthDayStyle ForeColor="#999999" /> 
                                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" /> 
                                    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" /> 
                                    <TitleStyle BackColor="#003399" Font-Size="10pt" BorderColor="#3366CC" Font-Bold="True" BorderWidth="1px" ForeColor="#CCCCFF" Height="25px" />
                                </asp:Calendar>
                                <asp:LinkButton ID="lbtnToday" runat="server" Text=":: Today " OnClick="lbtnToday_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnClearDate" runat="server" Text=":: Clear Date" OnClick="lbtnClearDate_Click"></asp:LinkButton>
                            </center>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <cc1:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="txtDueDate" PopupControlID="Panel1" Position="Bottom"></cc1:PopupControlExtender>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">System Type:</span>
                    <span style="color: red; font-weight: bold; font-size: 9pt;">* </span>
                    <asp:DropDownList ID="txtSystemType" runat="server"></asp:DropDownList>
                </div>
                <div class="addSpace">
                    <span class="labelColumn">Sales Rep:</span>
                    <span style="color: red; font-weight: bold; font-size: 9pt;">* </span>
                    <asp:Label ID="txtSalesRep" runat="server"></asp:Label><br />
                    <asp:Button ID="btnAddQuote" runat="server" Text="Start Quote" Style="float: right; margin-right: 10%;" OnClick="btnAddQuote_Click" />
                </div>


            </div>
        </div>
        <div class="rightSide hidden" id="hiddenDetails" runat="server">
            <div class="addLineItems">
                <span class="labelColumn">Part Number:
                <asp:DropDownList ID="ddlPartNumber" runat="server" class="maxlabelcolumn"></asp:DropDownList></span>
                <span style="display:inline-block;width:250px;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <span >QTY:
                        <asp:TextBox ID="txtQuantity" runat="server" TextMode="Number" Width="30px" OnTextChanged="txtQuantity_TextChanged" AutoPostBack="true"></asp:TextBox></span>
                        <span >EXT Cost:
                        <asp:Label ID="lblLineTotal" runat="server" Text="0.00"></asp:Label></span>
                    </ContentTemplate>
                </asp:UpdatePanel></span>
                <asp:Button ID="btnAddItem" runat="server" Text="Add Item"  OnClick="btnAddItem_Click" />
            </div>
            <br />
            <div class="lineItemGV">
                <br />
                <asp:GridView ID="gvLineItems" runat="server" AutoGenerateColumns="False" Width="700px" EmptyDataText="ADD LINE ITEMS" 
                    BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    OnRowEditing="gvLineItems_RowEditing" OnRowCancelingEdit="gvLineItems_RowCancelingEdit" OnSelectedIndexChanged="gvLineItems_SelectedIndexChanged" 
                    OnRowUpdating="gvLineItems_RowUpdating" OnRowDeleting="gvLineItems_RowDeleting" DataKeyNames="ITEM_ID, QUOTE_ITEMS_QUOTE_FK, QUOTE_ITEMS_ITEM_FK, QUOTE_ITEMS_ID">
                    <Columns>
                        <asp:TemplateField HeaderText="Qty">
                            <ItemTemplate><%# Eval("QUOTE_ITEMS_ItemQty") %></ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtGVEditQTY" Width="20px" runat="server" TextMode="Number" Text=<%# Eval("QUOTE_ITEMS_ItemQty") %>></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Part #">
                            <ItemTemplate><%# Eval("ITEM_PartNumber") %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate><%# Eval("ITEM_Description") %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cost">
                            <ItemTemplate><%# Eval("ITEM_Cost") %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ext Cost">
                            <ItemTemplate><%# Eval("[EXT Cost]") %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="true" />
                        <asp:CommandField ShowDeleteButton="true" />
                    </Columns>
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SortedAscendingCellStyle BackColor="#EDF6F6" />
                    <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                    <SortedDescendingCellStyle BackColor="#D6DFDF" />
                    <SortedDescendingHeaderStyle BackColor="#002876" />
                </asp:GridView>
            </div>
            <br />
            <div class="additionalQuoteDetails">
                <div class="addSpace">
                    <span class="rightlabelcolumn">Sub Total:
                <asp:Label ID="lblQuoteSubTotal" runat="server" Text="0.00" BackColor="White" BorderColor="#333333" BorderStyle="Solid" BorderWidth="0"></asp:Label></span>
                </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                <div class="addSpace">
                    <span class="rightlabelcolumn">Drawings:
                <asp:TextBox ID="txtDrawings" runat="server" Text="300.00" OnTextChanged="txtDrawings_TextChanged" AutoPostBack="True"></asp:TextBox></span>
                </div>
                <div class="addSpace">

                            <span class="rightlabelcolumn">Markup Amount:
                <asp:Label ID="txtMarkupAmount" runat="server" Text="0.00"></asp:Label></span></div>
                            <%--                <cc1:DynamicPopulateExtender ID="DynamicPopulateExtender1" runat="server" BehaviorID="dp1" TargetControlID="txtMarkupAmount" PopulateTriggerControlID="SliderExtender1" ServiceMethod="DynamicPopulateExtender_onMarkupChange"></cc1:DynamicPopulateExtender>--%>
                            <div class="addSpace">
                                <span class="rightlabelcolumn">Markup %:
                <asp:TextBox ID="txtMarkupPercent" runat="server" Text="40" OnTextChanged="txtMarkupPercent_TextChanged" AutoPostBack="true"></asp:TextBox></span>
                            </div>
                            <%--                <div class="addSpace">
                    <span class="sliderclass">&nbsp
                <asp:TextBox ID="txtSlider" runat="server" ></asp:TextBox></span></div>
                <cc1:SliderExtender ID="SliderExtender1" runat="server" BehaviorID="Slider1" TargetControlID="txtSlider" BoundControlID="txtMarkupPercent" Minimum="0" Maximum="200"   Steps="200" Orientation="Horizontal" EnableHandleAnimation="true" TooltipText="Markup: Value = {0}"></cc1:SliderExtender>--%>



                            <div class="addSpace">
                                <span class="rightlabelcolumn">Labor Amount:
                <asp:TextBox ID="txtLabor" runat="server" Text="0.00" AutoPostBack="True" OnTextChanged="txtLabor_TextChanged"></asp:TextBox></span>
                            </div>
                            <div class="addSpace">
                                <span class="rightlabelcolumn">Shipping:
                <asp:TextBox ID="txtShipping" runat="server" Text="0.00" AutoPostBack="True" OnTextChanged="txtShipping_TextChanged"></asp:TextBox></span>
                            </div>
                            <div class="addSpace">
                                <span class="rightlabelcolumn">Total Cost:
                <asp:Label ID="lblQuoteTotal" runat="server" Text="0.00" BackColor="White" BorderColor="#333333" BorderStyle="Solid" BorderWidth="0"></asp:Label></span>
                            </div>
                            <br />
                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <span class="rightlabelcolumn">
                        <asp:Button ID="btnFinalize" runat="server" Text="Finalize Quote" OnClick="btnFinalize_Click" /></span>
    </div>
    </div>
            <div class="rightMargin">
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" runat="Server">
</asp:Content>

