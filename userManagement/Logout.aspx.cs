﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class userManagement_Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected override void OnInit(EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            Session["MyLogin"] = null;
            Session["myEmail"] = null;
            Session["isAdmin"] = null;
            Session["myInits"] = null;
        }
        Response.Redirect("../Default.aspx");
    }
}