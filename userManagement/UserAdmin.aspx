﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="UserAdmin.aspx.cs" Inherits="userManagement_UserAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" runat="Server" ClientIDMode="Static">
    
    <div id="accordian" style="clear:both;display:block;width:40%;float:left;">
        <h3>Reset Password</h3>
        <div id="passwordWrapper" style="display:block;float:left;">
            <div class="bottom" >
                <span>
                    <asp:Label ID="lblMsg" runat="server" Text="Please create a password (at least 8 characters)."></asp:Label>
                </span>

            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Password:
                </span>
                <span style="float: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* " ControlToValidate="txtPwd1" ForeColor="Red" Font-Size="9pt" ValidationGroup="pwdbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtPwd1" runat="server" TextMode="Password"></asp:TextBox>
                </span>
            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Retype Password:
                </span>
                <span style="float: left; margin-bottom: 5px;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* " ControlToValidate="txtPwd2" ForeColor="Red" Font-Size="9pt" ValidationGroup="pwdbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtPwd2" runat="server" TextMode="Password"></asp:TextBox>
                </span>
            </div>
            <br />
            <div class="bottom" style="margin-left: 300px;float:left;">
                <span></span>
                <span>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="pwdbox" />
                </span>
            </div>
        </div><%--end passwordWrapper--%>
        <h3 id="newUserAdmin" runat="server" class="hidden" style="float:left;clear:both;">Add New User</h3>
        <div id="newUserWrapper" runat="server" class="hidden" style="display:block;float:left;clear:both;">
            <div class="bottom" >
                <br />
                <span style="float: left; width: 200px;">First Name:</span>
                <span style="float: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* " ControlToValidate="txtFName" ForeColor="Red" Font-Size="9pt" ValidationGroup="newuserbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                </span>
            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Last Name:</span>
                <span style="float: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* " ControlToValidate="txtLName" ForeColor="Red" Font-Size="9pt" ValidationGroup="newuserbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                </span>
            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Initials:</span>
                <span style="float: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="* " ControlToValidate="txtInits" ForeColor="Red" Font-Size="9pt" ValidationGroup="newuserbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtInits" runat="server"></asp:TextBox>
                </span>
            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Email:</span>
                <span style="float: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="* " ControlToValidate="txtEmail" ForeColor="Red" Font-Size="9pt" ValidationGroup="newuserbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </span>
            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Password:</span>
                <span style="float: left;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="* " ControlToValidate="txtPassword" ForeColor="Red" Font-Size="9pt" ValidationGroup="newuserbox"></asp:RequiredFieldValidator><asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                </span>
            </div>
            <div class="bottom" >
                <span style="float: left; width: 200px;">Administrator:</span>
                <span style="float: left;">
                    <asp:CheckBox ID="txtIsAdmin" runat="server" Checked="false"></asp:CheckBox>
                </span>
            </div>
            <br />
            <div class="bottom" style="margin-left: 290px;float:left;">
                <span></span>
                <span>
                    <asp:Button ID="btnAddUser" runat="server" Text="Add User" OnClick="btnAddUser_Click" ValidationGroup="newuserbox" />
                </span>
            </div>

        </div><%--end newUserWrapper--%>
        <h3 id="deleteUserAdmin" runat="server" class="hidden" style="float:left;clear:both;display:block;">Disable User Account</h3>
        <br />
        <div id="deleteUserWrapper" runat="server" class="hidden" style="display:block;float:left;clear:both;">
            <div class="bottom" >
                <span style="float: left; width: 200px;">Email:</span>
                <span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="* " ControlToValidate="txtEmailtoRemove" ForeColor="Red" Font-Size="9pt" ValidationGroup="removebox"></asp:RequiredFieldValidator><asp:TextBox ID="TxtEMailtoRemove" runat="server"></asp:TextBox>
                </span>
            </div>
            <br />
            <div class="bottom" style="margin-left: 260px;float:left;">
                <span></span>
                <span>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Remove User" OnClick="btnDeActivateUser_Click" ValidationGroup="removebox" />
                </span>
            </div>


        </div><%--end deleteUserWrapper--%>
    </div><%--endtabs--%>
    <div id="userTable" style="float:left;clear:right;display:block;">
        <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Groove" BorderWidth="1px" CellPadding="7" GridLines="Vertical" Class="hidden" CellSpacing="3" Font-Size="14px" Font-Underline="False">
            <AlternatingRowStyle BackColor="#DCDCDC" />
            <Columns>
                <asp:TemplateField HeaderText="Last Name">
                     <ItemTemplate>
                        <%# Eval("USER_LName") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="First Name">
                    <ItemTemplate>
                        <%# Eval("USER_FName") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email">
                     <ItemTemplate>
                        <%# Eval("USER_EMail") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Admin">
                     <ItemTemplate>
                        <%# Eval("USER_IsAdmin") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active">
                     <ItemTemplate>
                        <%# Eval("USER_IsActive") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
    </div>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Passwords must be at least 8 characters long" ControlToValidate="txtPwd1" Display="None" ValidationExpression="^.{8,}$"></asp:RegularExpressionValidator>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords must match" ControlToCompare="txtPwd1" ControlToValidate="txtPwd2" Display="None"></asp:CompareValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" runat="Server">
</asp:Content>

