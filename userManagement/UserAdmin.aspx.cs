﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class userManagement_UserAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                Page.Validate();
            }
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
        if (Request.QueryString["Admin"] == "True")
        {
            newUserAdmin.Attributes.Remove("class");
            newUserWrapper.Attributes.Remove("class");
            deleteUserWrapper.Attributes.Remove("class");
            deleteUserAdmin.Attributes.Remove("class");
            gvUsers.Attributes.Remove("Class");
            gvUsers.Attributes.Add("Class", "userGV");
            reDrawTable();

        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        
        
            System.Data.DataTable tbl = CMethods.returnTable("SELECT * FROM USERS WHERE USER_EMail=@email", "@email", Request.QueryString["email"].Trim());

            BCryptHasher hasher = new BCryptHasher();
            string pw = txtPwd1.Text.Trim();
            string hash = Encoding.UTF8.GetString(hasher.ComputeHash(Encoding.UTF8.GetBytes(pw)));
            CMethods.executeNonQuery("UPDATE USERS SET USER_PWD=@pwd, USER_IsActive=@active WHERE USER_EMail=@email", "@pwd", hash, "@active", 1, "@email", Request.QueryString["email"].Trim());

            tbl = CMethods.returnTable("SELECT * FROM USERS WHERE USER_EMail=@email AND USER_PWD=@pwd",
                                              "@email", Request.QueryString["email"].Trim(), "@pwd", hash);

            CLogin myLogin = new CLogin((string)tbl.Rows[0]["USER_FName"], (string)tbl.Rows[0]["USER_LName"], (string)tbl.Rows[0]["USER_Initials"],
                                        (string)tbl.Rows[0]["USER_EMail"], (Guid)tbl.Rows[0]["USER_UID"],
                                        (bool)tbl.Rows[0]["USER_IsAdmin"]);
            Session["myLogin"] = myLogin;
            Session["myEmail"] = myLogin.Email;
            Session["isAdmin"] = myLogin.Admin;
            Session["myInits"] = myLogin.Inits;
            Response.Redirect("~/Dashboard.aspx");
    }
    protected void btnAddUser_Click(object sender, EventArgs e)
    {
        Guid g = Guid.NewGuid();
        string First = txtFName.Text.Trim();
        string Last = txtLName.Text.Trim();
        string Inits = txtInits.Text.Trim();
        string Email = txtEmail.Text.Trim();

        string Pwd = txtPassword.Text.Trim();
        BCryptHasher hasher = new BCryptHasher();
        
        string pwdhash = Encoding.UTF8.GetString(hasher.ComputeHash(Encoding.UTF8.GetBytes(Pwd)));
        
        int isAdmin = Convert.ToInt16(txtIsAdmin.Checked);

        CMethods.executeNonQuery("INSERT INTO USERS (USER_UID, USER_FName, USER_LName, USER_Initials, USER_EMail, USER_PWD, USER_IsAdmin, USER_IsActive) VALUES (@UID, @FName, @LName, @Inits, @EMail, @PWD, @Admin, @Active)", "@UID", g, "@FName", First, "@LName", Last, "@Inits", Inits, "@EMail", Email, "@PWD", pwdhash, "@Admin", isAdmin, "@Active", 1);

        txtFName.Text = null;
        txtLName.Text = null;
        txtInits.Text = null;
        txtEmail.Text = null;
        txtPassword.Text = null;
        txtIsAdmin.Checked = false;

        reDrawTable();
    }
    protected void btnDeActivateUser_Click(object sender, EventArgs e)
    {
        CMethods.executeNonQuery("UPDATE USERS SET USER_IsActive=@0 WHERE USER_EMail=@email","@0",0,"@email",TxtEMailtoRemove.Text.Trim());
        TxtEMailtoRemove.Text = null;
        reDrawTable();
    }
    protected void reDrawTable()
    {
        System.Data.DataTable tbl = CMethods.returnTable("SELECT * FROM USERS ORDER BY USER_LName ASC");
        gvUsers.DataSource = tbl;
        gvUsers.DataBind();
    }
}