﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="PriceQuote.aspx.cs" Inherits="PriceQuote" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" runat="Server">
    <div id="QuoteContainer">
        <br />
        <br />
        <hr />
        <div id="TitleBlockContainer">

            <asp:Label ID="Label1" runat="server" Text="Project Name:" Width="150px" Font-Size="18px" Font-Bold="true"></asp:Label>
            <asp:TextBox ID="ProjectName" runat="server" CssClass="QuoteTextBox"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Project Address:" Width="150px" Font-Size="18px" Font-Bold="true"></asp:Label>
            <asp:TextBox ID="ProjectAddress" runat="server" CssClass="QuoteTextBox"></asp:TextBox>
            <br />
            <asp:Label ID="Label3" runat="server" Text="Project City:" Width="150px" Font-Size="18px" Font-Bold="true"></asp:Label>
            <asp:TextBox ID="ProjectCity" runat="server" CssClass="QuoteTextBox"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="Project State:" Width="150px" Font-Size="18px" Font-Bold="true"></asp:Label>
            <asp:TextBox ID="ProjectState" runat="server" Class="QuoteTextBox"></asp:TextBox>
            <br />
            <asp:Label ID="Label5" runat="server" Text="Project Zip:" Width="150px" Font-Size="18px" Font-Bold="true"></asp:Label>
            <asp:TextBox ID="ProjectZip" runat="server" Class="QuoteTextBox"></asp:TextBox>

        </div>
        <div id="DetailsContainer">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" runat="Server">
    <asp:GridView ID="GVQuoteItems" runat="server" Font-Size="14px" AllowPaging="True" CellPadding="4" CellSpacing="10" GridLines="None" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" Class="QuotesTable" BorderColor="Fuchsia" BorderStyle="Solid" BorderWidth="1px">
        <AlternatingRowStyle BackColor="#CCCCCC" BorderColor="Fuchsia" BorderStyle="Solid" BorderWidth="1px" Font-Size="14px" />
        <Columns>
            <asp:TemplateField HeaderText="QUOTE #">
                <ItemTemplate>
                    <%# Eval("QUOTE_Number") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sum of Items">
                <ItemTemplate>
                    <%# Eval("QUOTE_SumOfItemsCost") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Est Labor Cost">
                <ItemTemplate>
                    <%# Eval("QUOTE_LaborCost") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Drawing Cost">
                <ItemTemplate>
                    <%# Eval("QUOTE_Drawings") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Shipping Cost">
                <ItemTemplate>
                    <%# Eval("QUOTE_Shipping") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />

            </asp:TemplateField>
            <asp:TemplateField HeaderText="System Type">
                <ItemTemplate>
                    <%# Eval("QUOTE_SystemType") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Price">
                <ItemTemplate>
                    <%# Eval("QUOTE_TotalPrice") %>
                </ItemTemplate>
                <ItemStyle Height="30px" Width="100px" />

            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ControlStyle-Width="100px" />
            <asp:CommandField ShowDeleteButton="True" ControlStyle-Width="100px"/>
            <asp:CommandField ShowSelectButton="True" ControlStyle-Width="100px" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="Provider" runat="server"></asp:SqlDataSource>
</asp:Content>

