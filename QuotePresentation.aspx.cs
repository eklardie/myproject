﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QuotePresentation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            if (!Page.IsPostBack)
            {
                Page.Validate();
                CLogin myInfo = (CLogin)Session["myLogin"];
                string quoteNumber = "";
                if (Request.Cookies["QuoteNumber"] != null)
                {
                    quoteNumber = Request.Cookies["QuoteNumber"].ToString();

                    DataTable QuoteTable = new DataTable();
                }

                if (Request.QueryString["Quote"] != null)
                {
                    Response.Cookies.Clear();
                    string quote = Request.QueryString["Quote"].Trim();
                    DataTable QuoteTable = fillLineItems(quote);
                    GridView1.DataSource = QuoteTable;
                    GridView1.DataBind();

                    lblsalesperson.Text = myInfo.FirstName + " " + myInfo.LastName;
                    lblquotedate.Text = DateTime.Now.ToShortDateString();


                    foreach (DataRow row in QuoteTable.AsEnumerable())
                    {
                        lblquotenumber.Text = row["QUOTE_Number"].ToString();
                        lblprojectname.Text = row["QUOTE_ProjectName"].ToString();
                        lblprojectnametwo.Text = row["QUOTE_ProjectNameTwo"].ToString();
                        lblprojectaddress.Text = row["QUOTE_ProjectAddress"].ToString();
                        lblprojectaddresstwo.Text = row["QUOTE_ProjectAddressTwo"].ToString();
                        lblcity.Text = row["QUOTE_City"].ToString();
                        lblstate.Text = row["QUOTE_State"].ToString();
                        lblzip.Text = row["QUOTE_Zip"].ToString();
                        lblquotetotal.Text = Convert.ToDecimal(row["QUOTE_TotalPrice"].ToString()).ToString("C");

                        switch (row["QUOTE_SystemType"].ToString())
                        {
                            case "FAS":
                                lblsystemtype.Text = "Fire Alarm System";
                                break;
                            case "FAA":
                                lblsystemtype.Text = "Fire Alarm Additions";
                                break;
                            case "SAS":
                                lblsystemtype.Text = "Security Alarm System";
                                break;
                            case "SAA":
                                lblsystemtype.Text = "Security Alarm Additions";
                                break;
                            case "CAS":
                                lblsystemtype.Text = "Card Access System";
                                break;
                            case "CAA":
                                lblsystemtype.Text = "Card Access Additions";
                                break;
                            case "NCS":
                                lblsystemtype.Text = "Nurse Call System";
                                break;
                            case "NCA":
                                lblsystemtype.Text = "Nurse Call Additions";
                                break;
                            case "GSS":
                                lblsystemtype.Text = "Gym Sound System";
                                break;
                            case "GSA":
                                lblsystemtype.Text = "Gym Sound Additions";
                                break;
                            case "MCS":
                                lblsystemtype.Text = "Master Clock System";
                                break;
                            case "MCA":
                                lblsystemtype.Text = "Master Clock Additions";
                                break;
                            case "AES":
                                lblsystemtype.Text = "Apartment Entry System";
                                break;
                            case "AEA":
                                lblsystemtype.Text = "Apartment Entry Additions";
                                break;
                            case "ENG":
                                lblsystemtype.Text = "En-Gauge Extinguisher Monitoring";
                                break;
                            case "EXT":
                                lblsystemtype.Text = "Fire Extinguishers";
                                break;
                            case "KSS":
                                lblsystemtype.Text = "Kitchen Suppression System";
                                break;
                            case "ELT":
                                lblsystemtype.Text = "Emergency Lights";
                                break;

                            default:
                                lblsystemtype.Text = "";
                                break;
                        }
                        


                    }
                    

                }
                else
                {
                   // QuoteTable = fillLineItems(quoteNumber);
                }

            }

        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }
    protected DataTable fillLineItems(string quotenumber)
    {
        string newcommand = "SELECT e.QUOTE_ITEMS_ItemQty, d.ITEM_PartNumber, d.ITEM_Description," +
                            "g.QUOTE_TotalPrice, g.QUOTE_Number, g.QUOTE_ProjectName, g.QUOTE_ProjectNameTwo, " +
                            "g.QUOTE_ProjectAddress, g.QUOTE_ProjectAddressTwo, g.QUOTE_City, g.QUOTE_State, g.QUOTE_Zip, " +
                            "g.QUOTE_SystemType " +
                            "FROM QUOTE_ITEMS e JOIN ITEMS d ON e.QUOTE_ITEMS_ITEM_FK = d.ITEM_ID " +
                            "JOIN QUOTES g ON g.QUOTE_ID=e.QUOTE_ITEMS_QUOTE_FK " +
                            
                            "WHERE g.QUOTE_Number=@myValue";

        DataTable tbl = CMethods.returnTable(newcommand, "@myValue", Convert.ToInt32(quotenumber));
        return tbl;
    }




}
