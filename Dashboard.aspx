﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="CreateQuote" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="pageContainer">
        <div class="topSpacing"><span class="colorMe">""</span></div>
        <div class="leftMargin">
            <span></span>
        </div>
        <div class="onePage">
            <div id="rptcntnr" class="reportcontainer">

            
            </div>
            <span class="infoLabels">
                <br />
            </span>

            <div class="projectGV">
                <asp:GridView ID="gvProjects" runat="server" AutoGenerateColumns="False" Class="projectinnergv" DataKeyNames="QUOTE_ID,QUOTE_Number,USER_ID,USER_Initials" OnSelectedIndexChanged="gvProjects_SelectedIndexChanged" PageSize="15" AllowPaging="True" AllowSorting="True" EditRowStyle-CssClass="gvprojectrow" HeaderStyle-HorizontalAlign="Center" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Center" RowStyle-CssClass="gvprojectrow" SelectedRowStyle-CssClass="gvprojectrow" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None" OnSorting="gvProjects_Sorting" OnPageIndexChanging="gvProjects_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="QUOTE #">
                            <ItemTemplate><%#Eval("QUOTE_Number") %></ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SYSTEM TYPE">
                            <ItemTemplate><%#Eval("QUOTE_SystemType") %></ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project Name" SortExpression="PROJECT_Name">
                            <ItemTemplate><%# Eval("QUOTE_ProjectName") +"</br>"+Eval("QUOTE_ProjectNameTwo") %></ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project Address">
                            <ItemTemplate><%# Eval("QUOTE_ProjectAddress")+"</br>"+Eval("QUOTE_ProjectAddressTwo") %></ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Project City" SortExpression="PROJECT_City">
                            <ItemTemplate><%# Eval("QUOTE_City") %></ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" SortExpression="PROJECT_State">
                            <ItemTemplate><%# Eval("QUOTE_State") %></ItemTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Zip Code">
                            <ItemTemplate><%# Eval("QUOTE_Zip") %></ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="REP" SortExpression="USER_Initials">
                            <ItemTemplate><%# Eval("USER_Initials") %></ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Price">
                            <ItemTemplate><%# Eval("QUOTE_TotalPrice") %></ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DUE DATE">
                            <ItemTemplate><%#Eval("QUOTE_DueDate") %></ItemTemplate>
                            <ItemStyle Width="70px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate><%# Eval("QUOTE_Disposition") %></ItemTemplate>
                            <EditItemTemplate>
                                <asp:RadioButtonList ID="rblStatus" runat="server">
                                    <asp:ListItem Value="OPEN" />
                                    <asp:ListItem Value="REVISED" />
                                    <asp:ListItem Value="CONVERTED" />
                                    <asp:ListItem Value="LOST" />
                                </asp:RadioButtonList>
                            </EditItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                    </Columns>
                    <EditRowStyle CssClass="gvprojectrow"></EditRowStyle>
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <HeaderStyle HorizontalAlign="Center" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF"></HeaderStyle>
                    <PagerSettings Position="TopAndBottom" PageButtonCount="5" Mode="NumericFirstLast"></PagerSettings>
                    <PagerStyle HorizontalAlign="Center" BackColor="#C6C3C6" ForeColor="Black" CssClass="pagenumbers" Font-Bold="True" Font-Size="12px"></PagerStyle>
                    <RowStyle CssClass="gvprojectrow" BackColor="#DEDFDE" ForeColor="Black"></RowStyle>
                    <SelectedRowStyle CssClass="gvprojectrow" BackColor="#9471DE" Font-Bold="True" ForeColor="White"></SelectedRowStyle>
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                </asp:GridView>
            </div>

        </div>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" runat="Server">
</asp:Content>

