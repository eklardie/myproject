﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    CLogin myLogin;

    public DateTime curDT = DateTime.Now;
    String myUrl = "http://localhost:56959/userManagement/Logout.aspx";
    public String logoUrl = "http://localhost:56959/images/banner_Main.png";
    


    protected void Page_Load(object sender, EventArgs e)
    {
        //UriBuilder builder = new UriBuilder(myUrl);
        
    }

    protected override void OnInit(EventArgs e)
    {
        if (Session["myLogin"] != null)
        {
            signinBtn.Text = "Sign Out";
            signinBtn.Attributes.Remove("class");
            signinBtn.Attributes.Remove("data-toggle");
            signinBtn.Attributes.Remove("href");
            myUrl = getBaseURL() + "/userManagement/Logout.aspx";
            signinBtn.Attributes.Add("href", myUrl);
            Aboutus.Text = "Dashboard";
            Aboutus.Attributes.Remove("href");
            Aboutus.Attributes.Add("href", getBaseURL() +"/Dashboard.aspx");
            Firealarms.Text = "Create Quote";
            Firealarms.Attributes.Remove("href");
            Firealarms.Attributes.Add("href", getBaseURL() +"/Quotation.aspx");
            Extinguishers.Text = "Edit Quote";
            Extinguishers.Attributes.Remove("href");
            Extinguishers.Attributes.Add("href", "PUT_LINK_TO_EDIT_QUOTES_HERE.aspx");
            Extinguishers.Attributes.Add("style", "Display:none;");
            Kitchensuppression.Text = "User Admin";
            Kitchensuppression.Attributes.Remove("href");
            Kitchensuppression.Attributes.Add("href", getBaseURL() + "/userManagement/UserAdmin.aspx?Email="+Session["myEmail"]+"&Admin="+Session["isAdmin"]);
            Security.Text = "Items";
            Security.Attributes.Remove("href");
            Security.Attributes.Add("href", getBaseURL() + "/Items.aspx");
            Contactus.Text = "Search";
            Contactus.Attributes.Remove("href");
            Contactus.Attributes.Add("href", getBaseURL() + "/Search.aspx");
            //bannerMain.Attributes.Remove("src");
            //logoUrl = getBaseURL() + "/images/banner_Main.png";
            //bannerMain.Attributes.Add("src", logoUrl);
        }
    }
        protected void commit_click(object sender, EventArgs e) 
        {
            DataTable tbl1 = CMethods.returnTable("SELECT USER_EMail,USER_PWD FROM USERS WHERE USER_EMail=@email AND USER_IsActive=@1", "@email", username.Text.Trim(), "@1", 1);
            if (tbl1.Rows.Count > 0)
            {
                BCryptHasher hasher = new BCryptHasher();
                string pw = password.Text.Trim();
                if (hasher.Verify(pw, (string)tbl1.Rows[0][1]))
                {
                    System.Data.DataTable tbl = CMethods.returnTable("SELECT * FROM USERS WHERE USER_EMail=@email", "@email", username.Text);
                    CLogin myLogin = new CLogin((string)tbl.Rows[0]["USER_FName"], (string)tbl.Rows[0]["USER_LName"], (string)tbl.Rows[0]["USER_Initials"],
                        (string)tbl.Rows[0]["USER_EMail"], (Guid)tbl.Rows[0]["USER_UID"], (bool)tbl.Rows[0]["USER_IsAdmin"]);
                    Session["myLogin"] = myLogin;
                    Session["myEmail"] = myLogin.Email;
                    Session["isAdmin"] = myLogin.Admin;
                    Session["myInits"] = myLogin.Inits;

                    if (Request.QueryString["returnURL"] != null)
                    {
                        Response.Redirect("~/" + Request.QueryString["returnURL"]);

                    }
                    else
                    {
                        //This Should Redirect to a Dashboard / Option Menu ***CURRENTLY GOES TO THE CONFIGURE PRICE QUOTE PAGE**
                        Response.Redirect("~/Dashboard.aspx?usr="+myLogin.ID+"&admin="+myLogin.Admin);
                    }

                    signinBtn.Text = "Sign Out";
                    signinBtn.Attributes.Remove("class");
                    signinBtn.Attributes.Remove("data-toggle");
                    signinBtn.Attributes.Remove("href");
                    signinBtn.Attributes.Add("href", "userManagement/Logout.aspx");
                }

                //else
                //{
                //    Response.Redirect("~/userManagement/Login.aspx?msg=" + username.Text);
                //}
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void signout_click(object sender, EventArgs e)
        {
            Session["myLogin"] = null;
            signinBtn.Text = "Sign In";
            signinBtn.Attributes.Add("class", "dropdown-toggle");
            signinBtn.Attributes.Add("data-toggle", "dropdown");
            signinBtn.Attributes.Remove("OnClick");
        }

        private string getBaseURL()
        {
            string baseurl = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, 
                            HttpContext.Current.Request.ServerVariables["HTTP_HOST"], 
                            (HttpContext.Current.Request.ApplicationPath.Equals("/")) ? string.Empty 
                            : HttpContext.Current.Request.ApplicationPath);
            return baseurl;
        }
    
}
