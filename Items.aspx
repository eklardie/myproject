﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Items.aspx.cs" Inherits="Items" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="pageContainer">
        <div class="topSpacing"><span class="colorMe">""</span></div>
        <div class="leftMargin">
            <span></span>
        </div>
        <div class="fullPage">
            <span class="newclass">
                <br />
            </span>
            <span class="newclass">ADD / EDIT ITEMS</span><span style="width:30px;min-width=30px;">&nbsp</span><span id="errormessage" class="duplicate" runat="server"></span>
            <span class="newclass">
                <br />
            </span>
            <span class="newclass">
                <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="btnSearch_Click" />
                SEARCH BY ITEM NUMBER OR ITEM NAME:  
            </span>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            <span class="newclass"><br /></span>
            <span class="newclass">
                ADD NEW ITEM:  
            </span>
            <asp:Button ID="btnAddItem" runat="server" Text="ADD" OnClick="btnAddItem_Click" ValidationGroup="addItem" UseSubmitBehavior="True" />
            Part #: <asp:TextBox ID="TxtPartNumber" runat="server"></asp:TextBox>&nbsp;
            Description: <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>&nbsp;
            List Price: <asp:CompareValidator ID="CompareValidator1" runat="server" ValidationGroup="addItem" ErrorMessage="This Must be a valid Dollar Amount:" ForeColor="Red" Operator="DataTypeCheck" Type="Currency" ControlToValidate="txtListPrice" Font-Bold="True" Display="Dynamic"></asp:CompareValidator><asp:TextBox ID="txtListPrice" runat="server"></asp:TextBox>&nbsp;
            Unit Cost: <asp:CompareValidator ID="CompareValidator2" runat="server" ValidationGroup="addItem" ErrorMessage="This Must be a valid Dollar Amount:" ForeColor="Red" Operator="DataTypeCheck" Type="Currency" ControlToValidate="txtUnitCost" Font-Bold="true" Display="Dynamic"></asp:CompareValidator><asp:TextBox ID="txtUnitCost" runat="server" ></asp:TextBox>
            <span class="newclass"><br /></span>

            <div class="itemsgridstyle" >
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" Width="900" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowUpdating="GridView1_RowUpdating" DataKeyNames="ITEM_ID" AllowSorting="true" OnSorting="GridView1_Sorting">
                <Columns>
                    <asp:TemplateField HeaderText="PART NUMBER" SortExpression="ITEM_PartNumber">
                        <ItemTemplate><%# Eval("ITEM_PartNumber") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DESCRIPTION" SortExpression="ITEM_Description">
                        <ItemTemplate><%# Eval("ITEM_Description") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LIST PRICE">
                        <ItemTemplate><%# Eval("ITEM_ListPrice") %></ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVEditListPrice" runat="server" TextMode="Number" Text=<%# Eval("ITEM_ListPrice") %> ></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="COST">
                        <ItemTemplate><%# Eval("ITEM_Cost") %></ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtGVEditCost" runat="server" TextMode="Number" Text=<%# Eval("ITEM_Cost") %>></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ACTIVE">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" Checked=<%# Eval("ITEM_IsActive") %>/></ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked=<%# Eval("ITEM_IsActive") %>/>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <RowStyle BackColor="White" ForeColor="#003399" />
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <SortedAscendingCellStyle BackColor="#EDF6F6" />
                <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                <SortedDescendingCellStyle BackColor="#D6DFDF" />
                <SortedDescendingHeaderStyle BackColor="#002876" />
            </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" runat="Server">
</asp:Content>

