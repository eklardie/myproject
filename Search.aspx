﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">

        <div class="pageContainer">
        <div class="topSpacing"><span class="colorMe">""</span></div>
        <div class="leftMargin">
            <span></span>
        </div>
        <div class="fullPage">
            <span class="newclass">
                <br />
            </span>
            <span class="newclass">SEARCH PAGE</span><span style="width:30px;min-width=30px;">&nbsp</span><span id="errormessage" class="duplicate" runat="server"><asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">LinkButton</asp:LinkButton></span>
            <span class="newclass">
                <br />
            </span>
            <span class="newclass">
                
                SEARCH BY QUOTE NUMBER:  
            </span>
            <asp:TextBox ID="txtQuoteNumberSearch" runat="server"></asp:TextBox>
            <asp:Button ID="btnQuoteNumberSearch" runat="server" Text="SEARCH" OnClick="btnQuoteNumberSearch_Click" />
            <span class="newclass"><br /></span>
                        <span class="newclass">
                
                SEARCH BY PROJECT NAME:  
            </span>
            <asp:TextBox ID="txtProjectNameSearch" runat="server"></asp:TextBox>
            <asp:Button ID="btnProjectNameSearch" runat="server" Text="SEARCH" OnClick="btnProjectNameSearch_Click" />
            <span class="newclass">
                <br />
            </span>
            <div id="searchresultsgridview">

                <asp:GridView ID="GVSearchResults" runat="server"></asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" Runat="Server">
</asp:Content>

