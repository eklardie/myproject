﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphNavigation" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphContent" Runat="Server">
    <div id="collateralwrapper">

<div id="imageswrapper">

<div class="imageblock"><a href="firealarms"><img src="images/firealarmnotifierfamily.gif" alt="image" title="Click Here For Fire Alarm Systems" /></a></div>

<div class="imageblock"><a href="suppression"><img src="images/kitchensuppression.png" alt="Kitchen Suppression Systems" title="Click Here for Kitchen Suppression Systems" /></a></div>

<div class="imageblock"><a href="extinguishers"><img src="images/extinguishers2.png" alt="Fire Extinguishers" title="Click Here for Fire Extinguishers" /></a></div>

<div class="imageblock"><a href="systems"><img src="images/extinguishers2.png" alt="Emergency Lights" title="Click Here for Other Systems" /></a></div>

<div class="imageblock"><a href="security"><img src="images/security.png" alt="Security Systems" title="Click Here for Security Systems" style="height:auto;width:300px;"/></a></div>

</div>

<div id="textcollateralwrapper">

<p><a href="service">24/7 Emergency Service</a></p>

<p>Commercial and Industrial Systems</p>

<p><a href="http://www.concord.bbb.org" target="_blank">BBB Accredited</a></p>

<p><a href="http://www.nfpa.org" target="_blank">NFPA Member</a></p>
<p><a href="http://www.nicet.org">NICET Certified Technicians</a></p>

<p>25 Years in Business</p>

<p>Family Owned and Operated</p>

<p>Fully Licensed and Insured</p>

<p>Servicing New Hampshire, Maine, Massachusettes and Vermont</p>

<p><a href="http://www.notifier.com" target="_blank">Notifier Distributor</a></p>

</div></div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="containerDiv" Runat="Server">
    
</asp:Content>

